import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';
import { LoaderComponent } from './shared/component/loader/loader.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './shared/component/menu/menu.component';
import { ReportComponent } from './report/report.component';
import { FormComponent } from './form/form.component';
import { MasterUserComponent } from './master-user/master-user.component';
import { MasterDepartmentComponent } from './master-department/master-department.component';
import { ToastrModule } from 'ngx-toastr';
import {BsDatepickerModule, PaginationModule} from 'ngx-bootstrap';
import { MasterRscComponent } from './master-rsc/master-rsc.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { OnlyNumberDirective } from './shared/directive/only-number.directive';
import { FilterPipe } from './shared/pipe/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoaderComponent,
    HomeComponent,
    MenuComponent,
    ReportComponent,
    FormComponent,
    MasterUserComponent,
    MasterDepartmentComponent,
    MasterRscComponent,
    OnlyNumberDirective,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ToastrModule.forRoot(),
    PaginationModule.forRoot(),
    PaginationModule,
    SelectDropDownModule,
    BsDatepickerModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
