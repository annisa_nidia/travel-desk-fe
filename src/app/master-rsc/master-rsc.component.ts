import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ProjectServiceService } from '../shared/service/project-service.service';
import { Rsc } from '../shared/model/rsc';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

@Component({
  selector: 'app-master-rsc',
  templateUrl: './master-rsc.component.html',
  styleUrls: ['./master-rsc.component.css']
})
export class MasterRscComponent implements OnInit {


  loading = false;
  rscFormBuilder: FormGroup;
  listRSC: Rsc[] = [];
  totalRsc = 0;

  doOpenForm = false;
  doOpenConfirmation = false;
  successMessage = 'Data has been saved successfully';
  errorMesssage = 'An error occurred while saving data!';
  filterText = '';
  title = 'Master RSC';
  titleModal: string;
  getID: string;

  totalItems: number;
  currentPage = 1;
  itemsPerPage = 10;

  constructor(private formBuilder: FormBuilder, private service: ProjectServiceService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.rscFormBuilder = this.formBuilder.group({
      rscCode: ['', [Validators.required]],
      rscName: ['', [Validators.required]]
    });
    this.doGetListRSC();
  }

  // doGetTotalDept() {
  //   const param = {
  //
  //   };
  //   this.service.getListDepartment(param).subscribe((res) => {
  //     this.totalDept = res.recordsTotal;
  //     this.doGetListDept(1, this.totalDept);
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  doGetListRSC() {
    this.loading = true;
    const param = {
      ABC: ''
    };
    this.service.getListRSC(param).subscribe((res) => {
      this.loading = false;
      this.totalItems = res.recordsTotal;
      const data = res.data;
      this.listRSC = _.sortBy(data, ['rscCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving data!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.doOpenForm = true;
    this.titleModal = 'Add RSC';
    this.rscFormBuilder.reset();
  }

  doOpenFormEdit(rsc: Rsc) {
    this.doOpenForm = true;
    this.titleModal = 'Edit RSC';
    this.rscFormBuilder.get('rscCode').setValue(rsc.rscCode);
    this.rscFormBuilder.get('rscName').setValue(rsc.rscName);
  }

  doSubmit() {
    if (this.titleModal === 'Add RSC') {
      this.doInsertRSC();
    } else if (this.titleModal === 'Edit RSC' ) {
      this.doUpdateRSC();
    }
  }

  doInsertRSC() {
    if (this.rscFormBuilder.valid) {
      this.loading = true;
      const param: Rsc = this.rscFormBuilder.getRawValue();

      this.service.doInsertRSC(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListRSC();
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doUpdateRSC() {
    if (this.rscFormBuilder.valid) {
      this.loading = true;
      const param: Rsc = this.rscFormBuilder.getRawValue();

      this.service.doUpdateRSC(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListRSC();
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doDeleteConfirmation(id: string) {
    this.doOpenConfirmation = true;
    this.getID = id;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
    this.doGetListRSC();
  }
}
