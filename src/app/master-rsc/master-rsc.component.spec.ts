import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterRscComponent } from './master-rsc.component';

describe('MasterRscComponent', () => {
  let component: MasterRscComponent;
  let fixture: ComponentFixture<MasterRscComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterRscComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterRscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
