import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ProjectServiceService } from '../shared/service/project-service.service';
import { Rsc } from '../shared/model/rsc';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { User } from '../shared/model/user';
import { Department } from '../shared/model/department';

@Component({
  selector: 'app-master-user',
  templateUrl: './master-user.component.html',
  styleUrls: ['./master-user.component.css']
})
export class MasterUserComponent implements OnInit {



  loading = false;
  userFormBuilder: FormGroup;
  listUser: User[] = [];
  selectedRsc = [];
  selectedDepartment = [];
  listRSC: Rsc[] = [];
  listDepartment: Department[] = [];

  doOpenForm = false;
  doOpenConfirmation = false;
  successMessage = 'Data has been saved successfully';
  errorMesssage = 'An error occurred while saving data!';

  title = 'Master User';
  titleModal: string;
  getID: string;
  filterText = '';

  configSelectRsc = null;
  configSelectDepartment = null;

  totalItems: number;
  currentPage = 1;
  itemsPerPage = 10;
  totalUser = 0;

  constructor(private formBuilder: FormBuilder, private service: ProjectServiceService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.userFormBuilder = this.formBuilder.group({
      nik: ['', [Validators.required]],
      nama: ['', [Validators.required]],
      departmentCode: ['', [Validators.required]],
      rscCode: ['', [Validators.required]]
    });
    this.configSelectRsc = {
      displayKey: 'rscName',
      search: true,
      height: 'auto',
      placeholder: 'Select RSC',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search RSC',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectDepartment = {
      displayKey: 'departmentName',
      search: true,
      height: 'auto',
      placeholder: 'Select Department',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search Department',
      // searchOnKey: 'departmentCode'
    };
    this.doGetListUser(1, 10);
    this.doGetListRSC();
    this.doGetListDept();
  }

  doGetListRSC() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListRSC(param).subscribe((res) => {
      this.loading = false;
      const data = res.data;
      this.listRSC = _.sortBy(data, ['rscCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving RSC data!', 'Ooops!');
    });
  }

  doGetListDept() {
    this.loading = true;
    const param = {
      ABC: ''
    };
    this.service.getListDepartment(param).subscribe((res) => {
      this.loading = false;
      const data = res.data;
      this.listDepartment = _.sortBy(data, ['departmentCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving Department data!', 'Ooops!');
    });
  }

  // doGetTotalUser() {
  // const param = {
  //   deptCode: '',
  //   rscCode: ''
  // };
  //   this.service.getListUser(param).subscribe((res) => {
  //     this.totalUser = res.recordsTotal;
  //     this.doGetListUser(1, this.totalDept);
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  doGetListUser(start, end) {
    this.loading = true;
    const param = {
      deptCode: '',
      rscCode: ''
    };
    this.service.getListUser(param).subscribe((res) => {
      this.loading = false;
      this.totalItems = res.recordsTotal;
      const data = res.data;
      this.listUser = _.sortBy(data, ['rscCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving data!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.doOpenForm = true;
    this.titleModal = 'Add User';
    this.userFormBuilder.reset();
    this.selectedDepartment = [];
    this.listDepartment = [...this.listDepartment];
    this.selectedRsc = [];
    this.listRSC = [...this.listRSC];
  }

  doOpenFormEdit(user: User) {
    this.doOpenForm = true;
    this.titleModal = 'Edit User';
    this.userFormBuilder.get('nik').setValue(user.nik);
    this.userFormBuilder.get('nama').setValue(user.nama);
    this.userFormBuilder.get('departmentCode').setValue(user.departmentCode);
    this.userFormBuilder.get('rscCode').setValue(user.rscCode);
    this.listDepartment = [...this.listDepartment];
    this.listRSC = [...this.listRSC];
    const arr = [];
    const dept: Department = {
      departmentCode: user.departmentCode,
      departmentName: user.departmentName
    };
    arr.push(dept);
    this.selectedDepartment = arr;
    const arr2 = [];
    const rsc: Rsc = {
      rscCode: user.rscCode,
      rscName: user.rscName
    };
    arr2.push(rsc);
    this.selectedRsc = arr2;

  }

  doSubmit() {
    if (this.titleModal === 'Add User') {
      this.doInsertUser();
    } else if (this.titleModal === 'Edit User' ) {
      this.doUpdateUser();
    }
  }

  doInsertUser() {
    if (this.userFormBuilder.valid) {
      this.loading = true;
      const param: Rsc = this.userFormBuilder.getRawValue();

      this.service.doInsertUser(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListUser(1, 10);
      }, (err) => {
        this.loading = false;
        this.toastr.error('Connection Error!', 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doUpdateUser() {
    if (this.userFormBuilder.valid) {
      this.loading = true;
      const param: Rsc = this.userFormBuilder.getRawValue();

      this.service.doUpdateUser(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListUser(1, 10);
      }, (err) => {
        this.loading = false;
        this.toastr.error('Connection Error!', 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doDeleteConfirmation(id: string) {
    this.doOpenConfirmation = true;
    this.getID = id;
  }

  setSelectedRSC() {
    this.userFormBuilder.get('rscCode').setValue(this.selectedRsc[0].rscCode);
  }

  setSelectedDepartment() {
    this.userFormBuilder.get('departmentCode').setValue(this.selectedDepartment[0].departmentCode);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
    this.doGetListUser(1, 10);
  }
}

