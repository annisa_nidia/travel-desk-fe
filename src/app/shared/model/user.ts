export class User {
    username: string;
    password: string;
    access: string;
    userNik: string;
    regionCode: string;
    areaCode: string;
    areaName: string;
    regionName: string;
    userLevel: string;
    userName: string;
    deptId: string;
    deptName: string;
    outletName: string;
    rscCode: string;
    rscName: string;
    departmentCode: string;
    departmentName: string;
    nik: string;
    nama: string;
}
