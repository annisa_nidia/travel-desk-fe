export class Trip {
  departmentName: string;
  actual: string;
  rute: string;
  keterangan: string;
  tahun: string;
  departmentCode: string;
  periode: string;
  accDate: string;
  nik: string;
  revisi: string;
  nominal: number;
  nama: string;
  rscName: string;
  rscCode: string;
  id: string;
  rscCodeRute: string;
  rscNameRute: string;
}
