export class DataUser {
    userNik: string;
    regionCode: string;
    areaCode: string;
    areaName: string;
    regionName: string;
    userLevel: string;
    userName: string;
    outletName: string;
}