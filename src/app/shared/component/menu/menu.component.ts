import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataUser } from '../../model/datauser';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  dataUser: DataUser;
  today = new Date();
  constructor(private router: Router) { }

  ngOnInit() {
    this.dataUser = JSON.parse(localStorage.getItem('user'));
    // console.log(this.dataUser.userLevel);
  }

  doLogout() {
    this.router.navigate(['login']);
    localStorage.clear();
  }

}
