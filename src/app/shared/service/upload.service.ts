import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient, HttpEvent, HttpEventType, HttpRequest} from '@angular/common/http';
import { config } from '../config/application-config';
import { User } from '../model/user';
import { DataUser } from '../model/datauser';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  dataUser: DataUser;
  constructor(private http: HttpClient) { }

  pushFileToStorage(file: File): Observable<any> {
    const formdata: FormData = new FormData();
    this.dataUser = JSON.parse(localStorage.getItem('user'));
    const user = this.dataUser.userName;

    formdata.append('file', file);
    formdata.append('user', user);

    // const req = new HttpRequest('POST', config.TRANSACTION_SERVICE_BASE_URL + '/upload.action', formdata, {
    //   reportProgress: true,
    //   responseType: 'json'
    // });
    const req = new HttpRequest('POST', config.TRANS_LOCAL_URL + '/upload.action', formdata, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }
}
