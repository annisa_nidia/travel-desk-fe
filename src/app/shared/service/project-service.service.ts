import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { Observable } from 'rxjs/index';
import { config } from '../config/application-config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectServiceService {

  constructor(private httpClient: HttpClient) { }

  getToken(userDetail: User): Observable<any> {
    return this.httpClient.post(config.MASTER_SERVICE_BASE_URL + '/login', userDetail);
  }

  getUserInfo(userDetail: User, header): Observable<any> {
    return this.httpClient.post(config.MASTER_SERVICE_BASE_URL + '/role.action', userDetail, {headers: header});
  }

  getListDepartment(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/list-dept-json', param);
  }

  doInsertDepartment(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/save-dept.action', param);
  }

  doUpdateDepartment(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/update-dept.action', param);
  }

  getListRSC(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/list-rsc-json', param);
  }

  doInsertRSC(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/save-rsc.action', param);
  }

  doUpdateRSC(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/update-rsc.action', param);
  }

  getListUser(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/list-user-json', param);
  }

  doInsertUser(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/save-user.action', param);
  }

  doUpdateUser(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/update-user.action', param);
  }

  getListTransaction(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/report-json', param);
  }

  doInsertTransaction(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/save-transaction.action', param);
  }

  doUpdateTransaction(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/update-transaction.action', param);
  }

  doDeleteTransaction(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/delete.action', param);
  }

  getReport(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/report-periode-json', param);
  }

  getReportWithPaging(param): Observable<any> {
    return this.httpClient.post(config.TRANSACTION_SERVICE_BASE_URL + '/report-paging-json', param);
  }
}
