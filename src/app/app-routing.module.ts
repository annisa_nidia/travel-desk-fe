import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ReportComponent } from './report/report.component';
import { FormComponent } from './form/form.component';
import { MasterUserComponent } from './master-user/master-user.component';
import { MasterDepartmentComponent } from './master-department/master-department.component';
import { MasterRscComponent } from './master-rsc/master-rsc.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent, children: [
      {path: '', component: FormComponent},
      {path: 'report', component: ReportComponent},
      {path: 'master/user', component: MasterUserComponent},
      {path: 'master/department', component: MasterDepartmentComponent},
      {path: 'master/rsc', component: MasterRscComponent},
  ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {useHash: true}
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
