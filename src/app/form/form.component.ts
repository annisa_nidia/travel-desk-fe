import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ProjectServiceService } from '../shared/service/project-service.service';
import { Department } from '../shared/model/department';
import { ToastrService } from 'ngx-toastr';
import { Rsc } from '../shared/model/rsc';
import { User } from '../shared/model/user';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as numeral from 'numeral';
import { Trip } from '../shared/model/trip';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { UploadService } from '../shared/service/upload.service';
import {config} from "../shared/config/application-config";
import {BsDatepickerConfig} from "ngx-bootstrap";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [DatePipe]
})
export class FormComponent implements OnInit {
  @ViewChild('fileUpload')
  fileUpload: ElementRef;

  selectedFiles: FileList;
  currentFileUpload: File;

  loading = false;

  tripFormBuilder: FormGroup;

  listTrip: Trip[] = [];
  listRSC: Rsc[] = [];
  listUser: User[] = [];
  listDepartment: Department[] = [];
  selectedRSC: Rsc[] = [];
  selectedUser: User[] = [];
  selectedFilterRSC: Rsc[] = [];
  selectedFilterDepartment: Department[] = [];

  accDate: Date;
  fromDate: Date = new Date();
  toDate: Date = new Date();

  doOpenForm = false;
  doOpenConfirmation = false;
  doOpenModalImport = false;

  successMessage = 'Data has been saved successfully';
  errorMesssage = 'An error occurred while saving data!';
  title = 'Travelling Data';
  titleModal: string;
  getID: string;
  configSelectUser = null;
  configSelectRsc = null;
  configFilterRSC = null;
  configFilterDepartment = null;
  selectedActual = '';
  selectedTrip = null;
  urlExport = '';

  totalNominal = 0;
  totalRevisi = 0;
  totalItems: number;
  currentPage = 1;
  itemsPerPage = 10;
  numPages = 0;
  progress: { percentage: number } = {percentage: 0};

  public dpConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();

  constructor(private formBuilder: FormBuilder, private service: ProjectServiceService, private datePipe: DatePipe,
              private toastr: ToastrService, private uploadService: UploadService) {
    this.dpConfig.containerClass = 'theme-dark-blue';
    this.dpConfig.showWeekNumbers = false;
  }

  ngOnInit(): void {
    this.tripFormBuilder = this.formBuilder.group({
      nik: ['', [Validators.required]],
      actual: ['', [Validators.required]],
      rute: ['', [Validators.required]],
      nominal: ['', [Validators.required]],
      revisi: [''],
      periode: ['', [Validators.required]],
      tahun: ['', [Validators.required]],
      keterangan: ['', [Validators.required]],
      accDate: ['', [Validators.required]],
      rscCodeRute: ['', [Validators.required]]
    });
    this.configSelectUser = {
      displayKey: 'nama',
      search: true,
      height: 'auto',
      placeholder: 'Select User',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search User',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectRsc = {
      displayKey: 'rscName',
      search: true,
      height: 'auto',
      placeholder: 'Select RSC',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search RSC',
      // searchOnKey: 'departmentCode'
    };
    this.configFilterRSC = {
      displayKey: 'rscName',
      search: true,
      height: 'auto',
      placeholder: 'All RSC',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search RSC',
      // searchOnKey: 'departmentCode'
    };
    this.configFilterDepartment = {
      displayKey: 'departmentName',
      search: true,
      height: 'auto',
      placeholder: 'All Department',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'No results found!',
      searchPlaceholder: 'Search Department',
      // searchOnKey: 'departmentCode'
    };
    this.doGetListRSC();
    this.doGetListDept();
    this.doGetListTrip('', '');
    this.doGetListUser('');
  }

  doGetListRSC() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListRSC(param).subscribe((res) => {
      this.loading = false;
      const data = res.data;
      this.listRSC = _.sortBy(data, ['rscCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving RSC data!', 'Ooops!');
    });
  }

  doGetListDept() {
    this.loading = true;
    const param = {
      ABC: ''
    };
    this.service.getListDepartment(param).subscribe((res) => {
      this.loading = false;
      const data = res.data;
      this.listDepartment = _.sortBy(data, ['departmentCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving Department data!', 'Ooops!');
    });
  }

  doGetListUser(rscCode) {
    const param = {
      deptCode: '',
      rscCode: rscCode
    };
    this.service.getListUser(param).subscribe((res) => {
      const data = res.data;
      this.listUser = _.sortBy(data, ['nama']);
    }, (err) => {
      this.toastr.error('An error occurred while retrieving User data!', 'Ooops!');
    });
  }

  doGetListTrip(rsc, dept) {
    this.loading = true;
    this.totalRevisi = 0;
    this.totalNominal = 0;
    const param = {
      page: {
        start: ((this.currentPage - 1) * this.itemsPerPage) + 1,
        length: this.itemsPerPage,
        page: this.currentPage
      },
      deptCode: dept,
      rscCode: rsc,
      from: this.datePipe.transform(this.fromDate, 'dd MMM yyyy'),
      to: this.datePipe.transform(this.toDate, 'dd MMM yyyy')
    };
    this.service.getReportWithPaging(param).subscribe((res) => {
      this.loading = false;
      this.totalItems = res.recordsTotal;
      this.listTrip = res.data;
      for (let x = 0; x < res.data.length; x++) {
        const data = res.data[x];
        const nominal = data.nominal ? data.nominal : 0;
        const revisi = data.revisi ? data.revisi : 0;
        this.totalNominal = parseFloat(this.totalNominal.toString()) + parseFloat(nominal.toString());
        this.totalRevisi = parseFloat(this.totalRevisi.toString()) + parseFloat(revisi.toString());
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving Trip data!', 'Ooops!');
    });
  }

  doOpenFormAddTrip() {
    this.doOpenForm = true;
    this.titleModal = 'Add Trip';
    this.tripFormBuilder.reset();
    this.selectedRSC = [];
    this.listRSC = [...this.listRSC];
    this.selectedUser = [];
    this.listUser = [...this.listUser];
  }

  doOpenFormEditTrip(trip: Trip) {
    this.selectedTrip = trip;
    this.doOpenForm = true;
    this.titleModal = 'Edit Trip';
    this.listRSC = [...this.listRSC];
    this.listUser = [...this.listUser];
    this.tripFormBuilder.get('rscCodeRute').setValue(trip.rscCodeRute);
    this.tripFormBuilder.get('nik').setValue(trip.nik);
    this.tripFormBuilder.get('rute').setValue(trip.rute);
    this.tripFormBuilder.get('periode').setValue(trip.periode);
    this.tripFormBuilder.get('nominal').setValue(trip.nominal);
    this.tripFormBuilder.get('revisi').setValue(numeral(Number(trip.revisi)).format('0,0'));
    this.tripFormBuilder.get('tahun').setValue(trip.tahun);
    this.tripFormBuilder.get('accDate').setValue(moment(trip.accDate).toDate());
    this.tripFormBuilder.get('nominal').setValue(numeral(Number(trip.nominal)).format('0,0'));
    this.tripFormBuilder.get('actual').setValue(trip.actual);
    this.tripFormBuilder.get('keterangan').setValue(trip.keterangan);
    this.selectedActual = trip.actual;
    const arr = [];
    const user: any = {
      nik: trip.nik,
      nama: trip.nama
    };
    arr.push(user);
    this.selectedUser = arr;
    const arr2 = [];
    const rsc: Rsc = {
      rscCode: trip.rscCodeRute,
      rscName: trip.rscNameRute
    };
    arr2.push(rsc);
    this.selectedRSC = arr2;
  }

  doSubmit() {
    if (this.titleModal === 'Add Trip') {
      this.doInsertTrip();
    } else if (this.titleModal === 'Edit Trip' ) {
      this.doUpdateTrip();
    }
  }

  doInsertTrip() {
    if (this.tripFormBuilder.valid) {
      this.loading = true;
      const param: any = this.tripFormBuilder.getRawValue();
      param.accDate = this.datePipe.transform(this.accDate, 'dd MMM yyyy');
      param.nominal = Number(this.tripFormBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''));
      param.revisi = !this.tripFormBuilder.get('revisi').value ? '' : Number(this.tripFormBuilder.get('revisi').value.toString().replace(new RegExp(',', 'g'), ''));
      // console.log(param);
      this.service.doInsertTransaction(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
        const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
        this.doGetListTrip(rsc, dept);
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doUpdateTrip() {
    if (this.tripFormBuilder.valid) {
      this.loading = true;
      const transaction: any = this.tripFormBuilder.getRawValue();
      transaction.accDate = this.datePipe.transform(this.accDate, 'dd MMM yyyy');
      transaction.nominal = Number(this.tripFormBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''));
      transaction.revisi = !this.tripFormBuilder.get('revisi').value ? '' : Number(this.tripFormBuilder.get('revisi').value.toString().replace(new RegExp(',', 'g'), ''));
      transaction.id = this.selectedTrip.id;
      const log: any = {
        nikOld: this.selectedTrip.nik,
        actualOld: this.selectedTrip.actual,
        ruteOld: this.selectedTrip.rute,
        nominalOld: this.selectedTrip.nominal,
        revisiOld: this.selectedTrip.revisi,
        periodeOld: this.selectedTrip.periode,
        tahunOld: this.selectedTrip.tahun,
        accDateOld: this.datePipe.transform(this.selectedTrip.accDate, 'dd MMM yyyy'),
        keteranganOld: this.selectedTrip.keterangan,
        id: this.selectedTrip.id,
        rscCodeRute: this.selectedTrip.rscCodeRute
      };
      const param: any = {
        transaction: transaction,
        log: log
      };
      // console.log(param);
      this.service.doUpdateTransaction(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
        const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
        this.doGetListTrip(rsc, dept);
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doDeleteTrip() {
    const param: any = {
      id: this.getID
    };
    this.service.doDeleteTransaction(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success('Data has been deleted successfully', 'Success!');
      } else {
        this.toastr.error('An error occurred while deleting data!', 'Ooops!');
      }
      const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
      const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
      this.doGetListTrip(rsc, dept);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Connection Error!', 'Ooops!');
    });
  }

  doDeleteConfirmation(id: string) {
    this.doOpenConfirmation = true;
    this.getID = id;
  }

  doOpenFormImport() {
    this.doOpenModalImport = true;
    this.fileUpload.nativeElement.value = '';
    this.currentFileUpload = undefined;
    this.selectedFiles = undefined;
  }

  setSelectedRSC(event) {
    // this.doGetListUser(this.selectedRSC[0].rscCode);
    this.tripFormBuilder.get('rscCodeRute').setValue(this.selectedRSC[0].rscCode);
  }

  setSelectedUser() {
    this.tripFormBuilder.get('nik').setValue(this.selectedUser[0].nik);
  }

  getFilterResult() {
    this.currentPage = 1;
    const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
    const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
    this.doGetListTrip(rsc, dept);
    this.listRSC = [...this.listRSC];
    this.listDepartment = [...this.listDepartment];
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
    const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
    this.doGetListTrip(rsc, dept);
  }

  selectFile(event) {
    this.currentFileUpload = undefined;
    this.selectedFiles = undefined;
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.selectedFiles = undefined;
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        //noinspection TypeScriptUnresolvedVariable
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.toastr.success('File has been uploaded Successfully!', 'Success!');
      }
    }, (err) => {
      this.toastr.error('Upload Error...!', 'Ooops!');
    }, () => {
      this.fileUpload.nativeElement.value = '';
      this.currentFileUpload = undefined;
    });

  }

  doExport() {
    const rsc = this.selectedFilterRSC.length === 0 ? '' : this.selectedFilterRSC[0].rscCode;
    const dept = this.selectedFilterDepartment.length === 0 ? '' : this.selectedFilterDepartment[0].departmentCode;
    const from = encodeURI(this.datePipe.transform(this.fromDate, 'dd MMM yyyy'));
    const to =  encodeURI(this.datePipe.transform(this.toDate, 'dd MMM yyyy'));
    this.urlExport = config.TRANS_LOCAL_URL + '/report.xls?from=' + from  + '&to=' + to + '&rscCode=' + rsc + '&deptCode=' + dept;
    console.log(this.urlExport);
  }

  get actual() {
    return this.tripFormBuilder.get('actual');
  }
}

