import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { ProjectServiceService } from '../shared/service/project-service.service';
import { Department } from '../shared/model/department';
import { ToastrService } from 'ngx-toastr';

import * as _ from 'lodash';

@Component({
  selector: 'app-master-department',
  templateUrl: './master-department.component.html',
  styleUrls: ['./master-department.component.css']
})
export class MasterDepartmentComponent implements OnInit {

  loading = false;
  deptFormBuilder: FormGroup;
  listDept: Department[] = [];
  totalDept = 0;

  doOpenForm = false;
  doOpenConfirmation = false;
  successMessage = 'Data has been saved successfully';
  errorMesssage = 'An error occured while saving data!';

  title = 'Master Department';
  titleModal: string;
  getID: string;
  filterText = '';

  totalItems: number;
  currentPage = 1;
  itemsPerPage = 10;

  constructor(private formBuilder: FormBuilder, private service: ProjectServiceService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.deptFormBuilder = this.formBuilder.group({
      departmentCode: ['', [Validators.required]],
      departmentName: ['', [Validators.required]]
    });
    this.doGetListDept();
  }

  // doGetTotalDept() {
  //   const param = {
  //
  //   };
  //   this.service.getListDepartment(param).subscribe((res) => {
  //     this.totalDept = res.recordsTotal;
  //     this.doGetListDept(1, this.totalDept);
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  doGetListDept() {
    this.loading = true;
    const param = {
      ABC: ''
    };
    this.service.getListDepartment(param).subscribe((res) => {
      this.loading = false;
      this.totalItems = res.recordsTotal;
      const data = res.data;
      this.listDept = _.sortBy(data, ['departmentCode']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occurred while retrieving data!', 'Ooops!');
    });
  }

  doOpenFormAddDepartment() {
    this.doOpenForm = true;
    this.titleModal = 'Add Department';
    this.deptFormBuilder.reset();
  }

  doOpenFormEditDepartment(dept: Department) {
    this.doOpenForm = true;
    this.titleModal = 'Edit Department';
    this.deptFormBuilder.get('departmentCode').setValue(dept.departmentCode);
    this.deptFormBuilder.get('departmentName').setValue(dept.departmentName);
  }

  doSubmitDepartment() {
    if (this.titleModal === 'Add Department') {
      this.doInsertDepartment();
    } else if (this.titleModal === 'Edit Department' ) {
      this.doUpdateDepartment();
    }
  }

  doInsertDepartment() {
    if (this.deptFormBuilder.valid) {
      this.loading = true;
      const newDept: Department = this.deptFormBuilder.getRawValue();

      this.service.doInsertDepartment(newDept).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListDept();
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doUpdateDepartment() {
    if (this.deptFormBuilder.valid) {
      this.loading = true;
      const newDept: Department = this.deptFormBuilder.getRawValue();

      this.service.doUpdateDepartment(newDept).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
        // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
        this.doGetListDept();
      }, (err) => {
        this.loading = false;
        this.toastr.error(JSON.stringify(err), 'Ooops!');
      });
    } else {
      this.loading = false;
      this.toastr.error('Please complete required field!', 'Ooops!');
    }
  }

  doDeleteConfirmation(id: string) {
    this.doOpenConfirmation = true;
    this.getID = id;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    // this.doGetListDept(this.currentPage, (this.currentPage - 1) * this.itemsPerPage);
    this.doGetListDept();
  }
}
